# Mobilizer

Project is no longer mantained. It's a decently sized platform, backend built in 1 month and frontend in 1 month. Feel free to reuse something that looks helpful!

The mobilizer package is an open-sourced grass roots activism platform built on top the the Laravel Framework. It includes an HTML version of all pages as well as a RESTful API for creating a fancy SPA.

This repository is only the CRUD of the system, for a fully functional platform you will need to pull the Mobilizer-Python-Data repository which includes all mathematical algorithms for expanding and manipulating geospatial points as well as Elasticsearch synchronization. Our Python data repo is currently not public.

## Contributing

You are free to submit a pull request for bug fixes or new features. Major new features should be discussed prior to building.

## Security Vulnerabilities

If you discover a security vulnerability within the platform, please send an e-mail to Carlos at granados.carlos91@gmail.com. All security vulnerabilities will be promptly addressed.

## License

The Mobilizer platform and any derivatives from it, shall be strcily use for non commercial purposes only.